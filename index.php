<?php
require_once 'vendor/autoload.php';
use \giftbox\controler\ControlerCatalogue;
use \giftbox\models\Prestation;
use \giftbox\controler\ControlerCoffret;


$db = new \Illuminate\Database\Capsule\Manager();
//$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$param=parse_ini_file('src/conf/conf.ini');
$db->addConnection([
	'driver'=>$param['db_driver'],
	'host'=>$param['host'],
	'database'=>$param['dbname'],
	'username'=>$param['db_user'],
	'password'=>$param['db_password'],
	'charset'=>'utf8',
	'collation'=>'utf8_unicode_ci',
	'prefix'=>''
]);
$db->setAsGlobal();
$db->bootEloquent();

$app=new \Slim\Slim;
$app->get('/',function(){
	(new ControlerCatalogue())->accueil();
})->name('accueil');

/*
$app->get('/connexion', function(){
	(new ())->();
});
$app->get('/inscription', function(){
	(new ())->();
});
$app->get('/connexion/', function(){
	(new ())->();
});
*/

$app->get('/coffret/valider', function(){
	(new ControlerCoffret())->valider();
});
$app->get('/coffretEnr/:id', function($id){
	(new ControlerCoffret())->affichageEnr($id);
});
$app->get('/coffret/enregistrement/:num', function($num){
	(new ControlerCoffret())->donnerLien($num);
});
$app->get('/cat/', function(){
	(new ControlerCatalogue())->categories();
});
$app->get('/cat/:id',function($id){
	(new ControlerCatalogue())->categorie($id);
});
$app->get('/prest/', function(){
	(new ControlerCatalogue())->prestations();
});
$app->get('/prest/:id', function($id){
	(new ControlerCatalogue())->prestation($id);
});
$app->get('/ajout/:id', function($id){
	(new ControlerCoffret())->ajout($id);
});
$app->get('/coffret/', function(){
	(new ControlerCoffret())->affichage();
});
$app->get('/coffret/suppression/:num', function($num){
	(new ControlerCoffret())->suppression($num);
});


$app->run();
