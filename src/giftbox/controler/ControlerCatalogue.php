<?php
namespace giftbox\controler;
use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\vues\VueCatalogue;

class ControlerCatalogue {
	
	static function accueil(){
		(new VueCatalogue(null))->afficher(0);
	}
	
	static function categories(){
		$categorie=Categorie::get();
		(new VueCatalogue($categorie->toArray()))->afficher(2);
	}
	
	static function categorie($id){
		$categorie=Categorie::where('id','=',$id)->first();
		$presta=$categorie->hasMany('\giftbox\models\Prestation','cat_id')->get();
		(new VueCatalogue($presta->toArray()))->afficher(1);
	}
	
	static function prestations(){
		$presta=Prestation::orderBy('prix')->get();
		(new VueCatalogue($presta->toArray()))->afficher(1);
	}
	
	static function prestation($id){
		$presta=Prestation::where('id', '=', $id)->get();
		$cat=$presta[0]->belongsTo('\giftbox\models\Categorie','cat_id')->first();
		$presta[]=$cat;
		(new VueCatalogue($presta->toArray()))->afficher(3);
	}
	
	static function donnerNomCategorie($id){
		$cat=Categorie::where('id','=',$id)->first();
		return $cat->nom;
	}
}
