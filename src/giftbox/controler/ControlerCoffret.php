<?php
namespace giftbox\controler;
use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\vues\VueCatalogue;
use \giftbox\vues\VueCoffret;

class ControlerCoffret {
	
	function __construct(){
		global $app;
		$app = \Slim\Slim::getInstance();
	}
	
	static function affichage(){
		session_start();
		if(! isset($_SESSION['num'])){
			$_SESSION['num'] = 1;
			$_SESSION['prest'] = null;
			$_SESSION['count'] = 0;
		}
		
		$prestation = $_SESSION['prest'];
		$vue = new VueCoffret($prestation);
		$vue->afficher(0);
	}
	
	static function donnerLien($num){
		(new VueCoffret(null))->afficherLien($num);
	}
	
	static function affichageEnr($num){
		$coffretL= \giftbox\models\Coffret::where('id','=',$num)->get();
		foreach($coffretL as $coffret){
			$presta=Prestation::where('id','=',$coffret['id_prest'])->first();
			$nb=$coffret->nb;
			for( $i=0;$i<$nb;$i++){
				$prestation[]=$presta;
			}
		}
		(new VueCoffret($prestation))->afficher(0);
	}
	
	static function categories(){
		$categorie=Categorie::get();
		(new VueCatalogue($categorie->toArray()))->afficher(2);
	}
		
	static function suppression($id){
		global $app;
		$url=$app->urlFor('accueil');
		session_start();

		$verif = false;
		foreach($_SESSION['prest'] as $clef=>$value){
			if ($value['id'] == $id && $verif == false){
				unset($_SESSION['prest'][$clef]);
				$verif = true;
				$_SESSION['count']-=1;
			}
		}
		if($_SESSION['count']==0){
			$_SESSION['prest']=null;
		}
		$app->redirect("$url/coffret/1");
		
	}
	
	static function ajout($id){
		global $app;
		$url=$app->urlFor('accueil');
		session_start();
		if(! isset($_SESSION['num'])){
			$_SESSION['num'] = 1;
			$_SESSION['prest'] = null;
			$categorie=Categorie::where('id','=',$id)->first();
			$_SESSION['count'] = 1;
			$_SESSION['prest'][$_SESSION['count']] = $categorie;
		}
		else {
			$_SESSION['count'] += 1;
			$prest=Prestation::where('id','=',$id)->first();
			$_SESSION['prest'][$_SESSION['count']] = $prest;
		}
		$app->redirect("$url/prest");
	}
	
	static function valider(){
		global $app;
		$url=$app->urlFor("accueil");
		session_start();
		if($_SESSION['count']>=2){
			foreach($_SESSION['prest'] as $value){
				if(!isset($cat)){
					$cat=$value['cat_id'];
				}else{
					if($cat!=$value['cat_id']){
						$num=self::enregistrer();
						break;
					}
				}
			}
		}
		if(isset($num)){
			$app->redirect("$url/coffret/enregistrement/$num");
		}else{
			$app->redirect("$url/coffret");
		}
	}
	
	static function enregistrer(){
		$id= \giftbox\models\Coffret::max('id');
		$id+=1;
		foreach($_SESSION['prest'] as $clef=>$value){
			$coffret=\giftbox\models\Coffret::where('id','=',$id)->where('id_prest','=',$value['id'])->first();
			if(isset($coffret)){
				$coffret->nb+=1;
				$coffret->save();
			}else{
				$coffret= new \giftbox\models\Coffret();
				$coffret->id=$id;
				$coffret->id_prest=$value['id'];
				$coffret->nb=1;
				$coffret->save();
			}
		}
		return $id;
	}
}
