<?php
namespace giftbox\vues;
class VueCoffret {
	

	function __construct($tab){
		global $tableau;
		$tableau = $tab;
		global $app;
		$app = \Slim\Slim::getInstance();
	}
	
	private function entete($css){
		global $app;
		$url=$app->urlFor("accueil");
		$html=<<<END
		<!doctype html>
		<html lang="fr">
		<head>
			<meta charset="utf-8">
			<title>Giftbox</title>
			<link rel="stylesheet" href="$url/web/css/$css">
		</head>
		
		<body>
			<header>
				<h1>Giftbox (Basire/Chaffaut)</h1>
			</header>
			<nav>
				<ul>
					<li><a href="$url">Accueil</a></li>
					<li><a href="$url/cat/">Categories</a></li>
					<li><a href="$url/prest/">Prestations</a></li>
					<li><a href="$url/coffret/">Coffret</a></li>
				</ul>
			</nav>
			<div class=contenu>
		
		
END;
return $html;
	}
	
	private function afficherContenu(){
		$html= $this->entete('main.css');
		global $tableau;
		global $app;
		$url=$app->urlFor("accueil");
		if(isset($tableau)){
			if(empty($tableau)){
				$html .= <<<END
				<p class='panierVide'> Votre coffret est vide</p>
END;
			}else{
				$prix=0;
				foreach($tableau as $presta){
					$cat = \giftbox\controler\ControlerCatalogue::donnerNomCategorie($presta['cat_id']);
					$html .= <<<END
					<section class=prestation>
					<p><a href="$url/prest/$presta[id]">$presta[nom]</a> <a href="$url/cat/$presta[cat_id]"> $cat</a> <img src=$url/web/img/$presta[img] style=width:50px;height=50px;> $presta[prix] </p>
					<form action=$url/coffret/suppression/$presta[id]>
					<input type="submit" value="Supprimer">
					</form>
					</section>
					</br>
END;
					$prix+=$presta['prix'];
				}
				$html .= <<<END
				<p class=prixT> Prix total : $prix </p>
				<form action=$url/coffret/valider>
				<input type="submit" value="Valider le coffret" >
				</form>
END;
			}
		}else{
			$html .= <<<END
			<p class='panierVide'> Votre coffret est vide</p>
END;
		}
		$html.='</body></html>';
		return $html;
	}
	

	
	public function afficher($num){
		switch ($num){
			case 0:
				echo $this->afficherContenu();
				break;
			case 1:
				echo $this->afficherContenu();
				break;
		}
	}
	
	public function afficherLien($num){
		$html=$this->entete('main.css');
		global $app;
		$url=$app->urlFor('accueil');
		$html.=<<<END
		<p> Le lien du coffret que vous venez d'enregistrer est <a href="$url/coffretEnr/$num">https://webetu.iutnc.univ-lorraine.fr$url/coffretEnr/$num</a> </p></body></html>
END;
		echo $html;
	}
}
?>
