<?php
namespace giftbox\vues;
class VueCatalogue {
	

	function __construct($tab){
		global $tableau;
		$tableau = $tab;
		global $app;
		$app = \Slim\Slim::getInstance();
	}
	
	private function entete($css){
		global $app;
		$url=$app->urlFor('accueil');
		$html=<<<END
		<!doctype html>
		<html lang="fr">
		<head>
			<meta charset="utf-8">
			<title>Giftbox</title>
			<link rel="stylesheet" href="$url/web/css/$css">
		</head>
		
		<body>
			<header>
				<h1>Giftbox (Basire/Chaffaut)</h1>
			</header>
			<nav>
				<ul>
					<li><a href="$url">Accueil</a></li>
					<li><a href="$url/cat">Catégories</a></li>
					<li><a href="$url/prest/">Prestations</a></li>
					<li><a href="$url/coffret/">Coffret</a></li>
				</ul>
			</nav>
			<div class=contenu>	
		
END;
	return $html;
	}
	
	private function listePrestations(){
		$html= $this->entete('main.css');
		global $tableau;
		global $app;
		$url=$app->urlFor('accueil');
		// Vrai valeur à afficher
		// <p><a href="$url/prest/$presta[id]">$presta[nom]</a> <img src=$url/web/img/$presta[img] style=width:50px;height=50px;> $presta[prix] </p>
		
		
		foreach($tableau as $presta){
			$cat = \giftbox\controler\ControlerCatalogue::donnerNomCategorie($presta['cat_id']);
			$html .= <<<END
			<section class=prestation>
			<p><a href="$url/prest/$presta[id]">$presta[nom]</a> <a href="$url/cat/$presta[cat_id]"> $cat</a> <img src=$url/web/img/$presta[img] style=width:50px;height=50px;> $presta[prix] </p>
			<form action=$url/ajout/$presta[id]>
			<input type="submit" value="Ajouter au panier">
			</form>
			<br>
			</section>
END;
		}
		$html.='</div></body></html>';
		return $html;
	}
	
	private function categorie(){
		$html= $this->entete('main.css');
		global $tableau;
		global $app;
		$url=$app->urlFor('accueil');
		foreach($tableau as $cat){
			$html .= "<a href=$url/cat/$cat[id]> $cat[nom]</a><br>";
			}
		$html.='</body></html>';
		return $html;
	}
	
	private function prestation(){
		$html= $this->entete('main.css');
		global $tableau;
		global $app;
		$url=$app->urlFor('accueil');

		$presta=$tableau[0];
		$cat=$tableau[1];
		$html .= <<<END
		<section class=prestation>
		<p><a href=$url/$presta[id]>$presta[nom]</a> $presta[descr] <a href=$url/cat/$cat[id]>$cat[nom]</a> <img src=$url/web/img/$presta[img] style=width:50px;height=50px;> $presta[prix]</a> </p>
		<form action=$url/ajout/$presta[id]>
		<input type="submit" value="Ajouter au panier">
		</form>
		</section>		
		<br>
END;
		$html.='</body></html>';
		return $html;
	}
	
	private function accueil(){
		$html=$this->entete('main.css')."<p>Pas de contenu car la fonctionnalité des notes n'a pas été implémentée (ps: Veuillez pardonner cet affichage haut en couleur et fort laid)"."</body></html>";
		return $html;		
	}
	
	public function afficher($num){
		switch ($num){
			case 0:
				echo $this->accueil();
				break;
			case 1:
				echo $this->listePrestations();
				break;
			case 2:
				echo $this->categorie();
				break;
			case 3:
				echo $this->prestation();
				break;
		}
	}
	
	
	
	
}
?>