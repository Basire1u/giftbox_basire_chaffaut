<?php
namespace giftbox\models;
use illuminate\database\Eloquent\Model;

class Coffret extends Model {
	
	protected $table = "coffret";
	protected $primaryKey = 'id';
	public $timestamps= false;
}