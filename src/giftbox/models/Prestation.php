<?php
namespace giftbox\models;
use illuminate\database\Eloquent\Model;

class Prestation extends Model {
	
	protected $table = "prestation";
	protected $primaryKey = 'id';
	public $timestamps= false;
}