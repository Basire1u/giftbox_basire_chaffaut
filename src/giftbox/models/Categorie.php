<?php
namespace giftbox\models;
use illuminate\database\Eloquent\Model;

class Categorie extends Model {
	
	protected $table = "categorie";
	protected $primaryKey = 'id';
	public $timestamps=false;
}